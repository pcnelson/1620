
public class Main {

	public static void main(String[] args) {
		///Option3
		//Respond to exception at a higher level.
		try{
		wakeUp();
		
		getReadyForSchool();
		
		goToClass();
		
		eatLunch();
		
		goToMoreClass();
		
		goHome();
		}catch(AbductionException e)
		{
			System.out.println("Pete heard about the abduction on campus.");
			System.out.println("Pete goes back to sleep.");
		}

	}
	/*
	private static void wakeUp() {
		// TODO Auto-generated method stub
		
		System.out.println("Pete wakes up.");
		
		//Heres how to throw a new exception
		
		///Option1: ignore the exception and let the program terminate.		
		//throw new AbductionException();
		
		///Option2: catch the exception
		try{
			throw new AbductionException();
		}catch(AbductionException e)
		{
			System.out.println("Pete's teacher was abducted by aliens. WoooHOoooooo!");
			System.out.println("Pete goes back to bed");
		}
		
		
	}*/
	///Option3:
	private static void wakeUp() throws  {
		// TODO Auto-generated method stub
		
		System.out.println("Pete wakes up.");
		
		//Heres how to throw a new exception
		
		///Option1: ignore the exception and let the program terminate.		
		//throw new AbductionException();
		
		///Option2: catch the exception
		try{
			throw new AbductionException();
		}catch(AbductionException e)
		{
			System.out.println("Pete's teacher was abducted by aliens. WoooHOoooooo!");
			System.out.println("Pete goes back to bed");
		}
		
		
	}

	private static void getReadyForSchool() {
		// TODO Auto-generated method stub]
		
		System.out.println("Pete gets ready for school.");
		
		
	}

	private static void goToClass() {
		// TODO Auto-generated method stub
		
		System.out.println("Pete drives to class.");
		
	}

	private static void eatLunch() {
		// TODO Auto-generated method stub
		
		System.out.println("Pete eats lunch. He eats a slice of pizza.");
	}

	private static void goToMoreClass() {
		// TODO Auto-generated method stub
		
		System.out.println("Pete heads to his other classes.");
		
	}

	private static void goHome() {
		// TODO Auto-generated method stub
		
		System.out.println("Pete drives back home to play some vidja games.");
		
	}

}
