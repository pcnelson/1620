/**
 * Creating our own exception class
 * 
 * 2types of exceptions
 * checked - all checked exceptions have to be caught (extends Exception)
 * unchecked - dont have to be caught (extends RuntimeException)
 * @author Pete
 *
 */
public class AbductionException extends RuntimeException{

	
	
}
